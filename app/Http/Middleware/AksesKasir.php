<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class AksesKasir
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
					if (Auth::check() && $request->user()->type_petugas_id ==4) {
						return $next($request);
					}
     return redirect()->route('home');
    }
}
