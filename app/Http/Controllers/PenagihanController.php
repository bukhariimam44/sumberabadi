<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Jenssegers\Agent\Agent;
use App\MasterTruck;
use App\MasterTarif;
use App\User;
use App\Delivery;
use App\TransferReturn;
class PenagihanController extends Controller
{
	public function __construct()
	{
					$this->middleware('penagihan');
	}
	public function delivery(Request $request){
		$datas = Delivery::get();
		$agent = new Agent();
		return view('penagihan.'.($agent->isMobile() ? 'mobile' : 'desktop') .'.delivery',compact('datas'));
	}
	public function transfer_return(Request $request){
		$datas = TransferReturn::get();
		$agent = new Agent();
		return view('penagihan.'.($agent->isMobile() ? 'mobile' : 'desktop') .'.transfer_return',compact('datas'));
	}
	public function rekap(Request $request){
		$datas = TransferReturn::get();
		$agent = new Agent();
		return view('penagihan.'.($agent->isMobile() ? 'mobile' : 'desktop') .'.rekap',compact('datas'));
	}
}
