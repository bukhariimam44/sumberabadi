<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Jenssegers\Agent\Agent;
use App\MasterTruck;
use App\MasterTarif;
use App\User;
use App\Delivery;
use App\TransferReturn;

class AdminController extends Controller
{
		public function __construct()
		{
						$this->middleware('admin');
		}
		public function master_truck(Request $request)
		{
			$datas = MasterTruck::where('aktif','yes')->get();
			$agent = new Agent();
			return view('admin.'.($agent->isMobile() ? 'mobile' : 'desktop') .'.master_truck',compact('datas'));
		}
		public function master_tarif(Request $request){
			$datas = MasterTarif::where('aktif','yes')->get();
			$agent = new Agent();
			return view('admin.'.($agent->isMobile() ? 'mobile' : 'desktop') .'.master_tarif',compact('datas'));
		}
		public function master_user(Request $request){
			$datas = User::get();
			$agent = new Agent();
			return view('admin.'.($agent->isMobile() ? 'mobile' : 'desktop') .'.master_user',compact('datas'));
		}
		public function delivery(Request $request){
			$datas = Delivery::get();
			$agent = new Agent();
			return view('admin.'.($agent->isMobile() ? 'mobile' : 'desktop') .'.delivery',compact('datas'));
		}
		public function transfer_return(Request $request){
			$datas = TransferReturn::get();
			$agent = new Agent();
			return view('admin.'.($agent->isMobile() ? 'mobile' : 'desktop') .'.transfer_return',compact('datas'));
		}
		public function rekap(Request $request){
			$datas = TransferReturn::get();
			$agent = new Agent();
			return view('admin.'.($agent->isMobile() ? 'mobile' : 'desktop') .'.rekap',compact('datas'));
		}
}
