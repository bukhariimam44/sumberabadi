<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;

class UmumController extends Controller
{
    public function keluar(){
					Auth::guard()->logout();
					return redirect()->back();
				}
}
