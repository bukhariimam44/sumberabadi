<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Jenssegers\Agent\Agent;
use App\MasterTruck;
use App\MasterTarif;
use App\User;
use App\Delivery;
use App\TransferReturn;
class KasirController extends Controller
{
	public function __construct()
	{
					$this->middleware('kasir');
	}
		public function delivery(Request $request){
			$datas = Delivery::get();
			$agent = new Agent();
			return view('kasir.'.($agent->isMobile() ? 'mobile' : 'desktop') .'.delivery',compact('datas'));
		}
		public function transfer_return(Request $request){
			$datas = TransferReturn::get();
			$agent = new Agent();
			return view('kasir.'.($agent->isMobile() ? 'mobile' : 'desktop') .'.transfer_return',compact('datas'));
		}
		public function rekap(Request $request){
			$datas = TransferReturn::get();
			$agent = new Agent();
			return view('kasir.'.($agent->isMobile() ? 'mobile' : 'desktop') .'.rekap',compact('datas'));
		}
}
