<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterTruck extends Model
{
	protected $fillable = [
		'id', 'data_truck', 'type_truck', 'aktif', 'admin', 'created_at', 'updated_at'
	];
}
