<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterTarif extends Model
{
	protected $fillable = [
		'id', 'origin', 'type_truck','destination','end_of_route','shipment_type','deal','pay_to_drive','trip_base','p_dan_l','persen', 'aktif', 'admin', 'created_at', 'updated_at'
	];
}
