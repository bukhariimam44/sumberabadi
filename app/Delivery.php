<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
	protected $fillable = [
'periode', 'client', 'no_surat_jalan', 'no_do', 'destination_poin', 'city_or_area', 'item', 'bags', 'weight', 'type_truck', 'transporter', 'driver', 'sales', 'multi_drop_penagihan', 'return_drum_or_ibc', 'total_sales', 'trip_base', 'solar', 'overnight', 'multi_drop_kasir', 'total_cost', 'profit', 'Remark', 'admin', 'aktif', 'created_at', 'updated_at'
	];
	public function trukId(){
		return $this->belongsTo('App\MasterTruck','type_truck');
	}
}
