<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/master-truk', 'AdminController@master_truck')->name('admin-master-truk');
Route::get('/master-tarif', 'AdminController@master_tarif')->name('admin-master-tarif');
Route::get('/master-user', 'AdminController@master_user')->name('admin-master-user');
Route::get('/delivery', 'AdminController@delivery')->name('admin-delivery');
Route::get('/transfer-return', 'AdminController@transfer_return')->name('admin-transfer-return');
Route::get('/rekap', 'AdminController@rekap')->name('admin-rekap');
//LAPANGAN
Route::get('/l-delivery', 'LapanganController@delivery')->name('lapangan-delivery');
Route::get('/l-add-delivery', 'LapanganController@add_delivery')->name('lapangan-add-delivery');
Route::get('/l-transfer-return', 'LapanganController@transfer_return')->name('lapangan-transfer-return');
Route::get('/l-rekap', 'LapanganController@rekap')->name('lapangan-rekap');
//PENAGIHAN
Route::get('/p-delivery', 'PenagihanController@delivery')->name('penagihan-delivery');
Route::get('/p-transfer-return', 'PenagihanController@transfer_return')->name('penagihan-transfer-return');
Route::get('/p-rekap', 'PenagihanController@rekap')->name('penagihan-rekap');
//KASIR
Route::get('/k-delivery', 'KasirController@delivery')->name('kasir-delivery');
Route::get('/k-transfer-return', 'KasirController@transfer_return')->name('kasir-transfer-return');
Route::get('/k-rekap', 'KasirController@rekap')->name('kasir-rekap');

Route::get('/keluar', 'UmumController@keluar')->name('keluar');
