<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">{{config('app.name')}}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ucwords(strtolower(Auth::user()->name))}} <br>
											<span style="font-size:8px;">( {{Auth::user()->typePetugasId->type}} )</span>
										</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
											<li class="nav-item">
													<a href="{{route('home')}}" class="nav-link {{setActive(['home'])}}">
															<i class="nav-icon fas fa-tachometer-alt"></i>
															<p>
															Dashboard
															</p>
													</a>
											</li>
											
          @if(Auth::user()->type_petugas_id ==1)
          <li class="nav-item has-treeview {{setOpen(['admin-master-truk','admin-master-tarif','admin-master-user'])}}">
            <a href="#" class="nav-link {{setActive(['admin-master-truk','admin-master-tarif','admin-master-user'])}}">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                Data Master
                <i class="fas fa-angle-left right"></i>
                <!-- <span class="badge badge-info right">6</span> -->
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('admin-master-tarif')}}" class="nav-link {{setActive(['admin-master-tarif'])}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Master Tarif</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('admin-master-truk')}}" class="nav-link {{setActive(['admin-master-truk'])}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Master Truck</p>
                </a>
														</li>
														<li class="nav-item">
                <a href="{{route('admin-master-user')}}" class="nav-link {{setActive(['admin-master-user'])}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Master User</p>
                </a>
              </li>
            </ul>
										</li>
										<li class="nav-item">
            <a href="{{route('admin-delivery')}}" class="nav-link {{setActive(['admin-delivery'])}}">
              <i class="nav-icon fas fa-edit""></i>
              <p>
                Delivery
                <!-- <span class="right badge badge-danger">New</span> -->
              </p>
            </a>
										</li>
										<li class="nav-item">
            <a href="{{route('admin-transfer-return')}}" class="nav-link {{setActive(['admin-transfer-return'])}}">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Transfer & Return
                <!-- <span class="right badge badge-danger">New</span> -->
              </p>
            </a>
										</li>
										<li class="nav-item">
            <a href="{{route('admin-rekap')}}" class="nav-link {{setActive(['admin-rekap'])}}">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Rekap
                <!-- <span class="right badge badge-danger">New</span> -->
              </p>
            </a>
										</li>
										@elseif(Auth::user()->type_petugas_id == 2)
										<li class="nav-item">
            <a href="{{route('lapangan-delivery')}}" class="nav-link {{setActive(['lapangan-delivery'])}}">
              <i class="nav-icon fas fa-edit""></i>
              <p>
                Delivery
              </p>
            </a>
										</li>
										<li class="nav-item">
            <a href="{{route('lapangan-transfer-return')}}" class="nav-link {{setActive(['lapangan-transfer-return'])}}">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Transfer & Return
              </p>
            </a>
										</li>
										<li class="nav-item">
            <a href="{{route('lapangan-rekap')}}" class="nav-link {{setActive(['lapangan-rekap'])}}">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Rekap
              </p>
            </a>
										</li>
										@elseif(Auth::user()->type_petugas_id == 3)
										<li class="nav-item">
            <a href="{{route('penagihan-delivery')}}" class="nav-link {{setActive(['penagihan-delivery'])}}">
              <i class="nav-icon fas fa-edit""></i>
              <p>
                Delivery
              </p>
            </a>
										</li>
										<li class="nav-item">
            <a href="{{route('penagihan-transfer-return')}}" class="nav-link {{setActive(['penagihan-transfer-return'])}}">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Transfer & Return
              </p>
            </a>
										</li>
										<li class="nav-item">
            <a href="{{route('penagihan-rekap')}}" class="nav-link {{setActive(['penagihan-rekap'])}}">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Rekap
              </p>
            </a>
										</li>
										@else
										<li class="nav-item">
            <a href="{{route('kasir-delivery')}}" class="nav-link {{setActive(['kasir-delivery'])}}">
              <i class="nav-icon fas fa-edit""></i>
              <p>
                Delivery
              </p>
            </a>
										</li>
										<li class="nav-item">
            <a href="{{route('kasir-transfer-return')}}" class="nav-link {{setActive(['kasir-transfer-return'])}}">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Transfer & Return
              </p>
            </a>
										</li>
										<li class="nav-item">
            <a href="{{route('kasir-rekap')}}" class="nav-link {{setActive(['kasir-rekap'])}}">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Rekap
              </p>
            </a>
										</li>
										@endif
          <li class="nav-item">
            <a href="{{route('keluar')}}" class="nav-link {{setActive(['keluar'])}}">
              <i class="nav-icon fas fa-lock"></i>
              <p>
                Logout
                <!-- <span class="right badge badge-danger">New</span> -->
              </p>
            </a>
										</li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>