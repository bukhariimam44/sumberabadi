@extends('layouts.app')
@section('css')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
<!-- DataTables -->
<style>
	div.card-body{
		font-size:12px;
	}
	
	th.warna-bag-lapangan{
		background-color:#54e054;
	}
	th.warna-bag-penagihan{
		background-color:#54cbef;
	}
	th.warna-tot-sales{
		background-color:#636f7385;
	}
	th.warna-bag-kasir{
		background-color:#f3a00ab5;
	}
	th.warna-remarks{
		background-color:#cf0af3b5;
	}
	</style>
	
	<?php foreach ($datas as $key => $value) { ?>
@if($value['warna'] % 2 == 0)
<style>
	td.warna-bag-lapangan-{{$value['warna']}}{
		background-color:#54e0542b;
	}
	td.warna-bag-penagihan-{{$value['warna']}}{
		background-color:#54cbef42;
	}
	td.warna-tot-sales-{{$value['warna']}}{
		background-color:#636f730d;
	}
	td.warna-bag-kasir-{{$value['warna']}}{
		background-color:#f3a00a3b;
	}
	td.warna-remarks-{{$value['warna']}}{
		background-color:#cf0af326;
	}
	</style>
@else
<style>
	td.warna-bag-lapangan-{{$value['warna']}}{
		background-color:#54e05463;
	}
	td.warna-bag-penagihan-{{$value['warna']}}{
		background-color:#54cbef8a;
	}
	td.warna-tot-sales-{{$value['warna']}}{
		background-color:#aeb4b659;
	}
	td.warna-bag-kasir-{{$value['warna']}}{
		background-color:#f3a00a73;
	}
	td.warna-remarks-{{$value['warna']}}{
		background-color:#cf0af354;
	} 
	</style>
	@endif
	<?php };?>

@endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Delivery</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
              <li class="breadcrumb-item active">Delivery</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <!-- <div class="card-header">
                <h3 class="card-title">Data Master Truck</h3>
              </div> -->
              <!-- /.card-header -->
              <div class="card-body">
																<div class="table-responsive">
																			<table class="table" border="1" bordercolor="black" style="color:black;">
																					<thead>
																					<tr>
																					<th class="warna-tot-sales">Auto</th>
																					<th colspan="13" class="warna-bag-lapangan"><center>Diisi Bagian Lapangan</center> </th>
																					<th colspan="3" class="warna-bag-penagihan"><center>Diisi Bagian Penagihan</center></th>
																					<th class="warna-tot-sales">Auto</th>
																					<th colspan="4" class="warna-bag-kasir"><center>Diisi Bagian Kasir</center></th>
																					<th class="warna-tot-sales">Auto</th>
																					<th class="warna-tot-sales">Auto</th>
																					<th class="warna-remarks"></th>
																					</tr>
																					<tr> 
																							<th class="warna-tot-sales">No</th>
																							<th class="warna-bag-lapangan">Periode</th>
																							<th class="warna-bag-lapangan">Client</th>
																							<th class="warna-bag-lapangan">No.Surat Jln</th>
																							<th class="warna-bag-lapangan">NO.DO</th>
																							<th class="warna-bag-lapangan">Dest.Point</th>
																							<th class="warna-bag-lapangan">City/Area</th>
																							<th class="warna-bag-lapangan">Item</th>
																							<th class="warna-bag-lapangan">Bags</th>
																							<th class="warna-bag-lapangan">Weight(kg)</th>
																							<th class="warna-bag-lapangan">Type Truck</th>
																							<th class="warna-bag-lapangan">Truck No</th>
																							<th class="warna-bag-lapangan">Transporter</th>
																							<th class="warna-bag-lapangan">Driver</th>
																							<th class="warna-bag-penagihan">Sales</th>
																							<th class="warna-bag-penagihan">Multi Drop</th>
																							<th class="warna-bag-penagihan">Return Drum/IDC</th>
																							<th class="warna-tot-sales">Tot.Sales</th>
																							<th class="warna-bag-kasir">Trip Base</th>
																							<th class="warna-bag-kasir">Solar</th>
																							<th class="warna-bag-kasir">Over Night</th>
																							<th class="warna-bag-kasir">Multi Drop</th>
																							<th class="warna-tot-sales">Total Cost</th>
																							<th class="warna-tot-sales">Profit</th>
																							<th class="warna-remarks">Remarks</th>
																					</tr>
																					</thead>
																					<tbody>
																					<?php
																					foreach ($datas as $key => $value) {
																						if(isset($total[$value['warna']])) { 
																								$total[$value['warna']]++; 
																						}else{
																								$total[$value['warna']]=1; 
																						}	
																					}
																					$n=count($datas);
																					$cekinstansi="";
																					$no = 0;
																					for ($i=0; $i < $n; $i++) { 
																						$dt=$datas[$i];
																						?>
																						
																					<tr>
																					@if($cekinstansi!=$dt['warna'])
																					<?php $no++;?>
																						<td @if($total[$dt['warna']] > 1) rowspan="{{$total[$dt['warna']]}}" style="vertical-align:middle;" @endif class="warna-tot-sales-{{$dt['warna']}}">{{$no}}.</td>
																							<td @if($total[$dt['warna']] > 1) rowspan="{{$total[$dt['warna']]}}" style="vertical-align:middle;" @endif class="warna-bag-lapangan-{{$dt['warna']}}">{{$dt->periode}}</td>
																							<td @if($total[$dt['warna']] > 1) rowspan="{{$total[$dt['warna']]}}" style="vertical-align:middle;" @endif class="warna-bag-lapangan-{{$dt['warna']}}">{{$dt->client}}</td>
																							@endif		
																							<td class="warna-bag-lapangan-{{$dt['warna']}}">{{$dt->no_surat_jalan}}</td>
																							<td class="warna-bag-lapangan-{{$dt['warna']}}">{{$dt->no_do}}</td>
																							<td class="warna-bag-lapangan-{{$dt['warna']}}">{{$dt->destination_poin}}</td>
																							<td class="warna-bag-lapangan-{{$dt['warna']}}">{{$dt->city_or_area}}</td>
																							<td class="warna-bag-lapangan-{{$dt['warna']}}">{{$dt->item}}</td>
																							<td class="warna-bag-lapangan-{{$dt['warna']}}">{{$dt->bags}}</td>
																							<td class="warna-bag-lapangan-{{$dt['warna']}}">{{number_format($dt->weight,0,',','.')}}</td>
																							@if($cekinstansi!=$dt['warna'])
																							<td @if($total[$dt['warna']] > 1) rowspan="{{$total[$dt['warna']]}}" style="vertical-align:middle;" @endif class="warna-bag-lapangan-{{$dt['warna']}}">{{$dt->trukId->type_truck}}</td>
																							<td @if($total[$dt['warna']] > 1) rowspan="{{$total[$dt['warna']]}}" style="vertical-align:middle;" @endif class="warna-bag-lapangan-{{$dt['warna']}}">{{$dt->trukId->data_truck}}</td>
																							<td @if($total[$dt['warna']] > 1) rowspan="{{$total[$dt['warna']]}}" style="vertical-align:middle;" @endif class="warna-bag-lapangan-{{$dt['warna']}}">{{$dt->transporter}}</td>
																							<td @if($total[$dt['warna']] > 1) rowspan="{{$total[$dt['warna']]}}" style="vertical-align:middle;" @endif class="warna-bag-lapangan-{{$dt['warna']}}">{{$dt->driver}}</td>
																							<td @if($total[$dt['warna']] > 1) rowspan="{{$total[$dt['warna']]}}" style="vertical-align:middle;" @endif class="warna-bag-penagihan-{{$dt['warna']}}">{{number_format($dt->sales,0,',','.')}}</td>
																							<td @if($total[$dt['warna']] > 1) rowspan="{{$total[$dt['warna']]}}" style="vertical-align:middle;" @endif class="warna-bag-penagihan-{{$dt['warna']}}">{{number_format($dt->multi_drop_penagihan,0,',','.')}}</td>
																							<td @if($total[$dt['warna']] > 1) rowspan="{{$total[$dt['warna']]}}" style="vertical-align:middle;" @endif class="warna-bag-penagihan-{{$dt['warna']}}">{{number_format($dt->return_drum_or_ibc,0,',','.')}}</td>
																							<td @if($total[$dt['warna']] > 1) rowspan="{{$total[$dt['warna']]}}" style="vertical-align:middle;" @endif class="warna-tot-sales-{{$dt['warna']}}">{{number_format($tot_sales = $dt->sales+$dt->multi_drop_penagihan+$dt->return_drum_or_ibc,0,',','.')}}</td>
																							<td @if($total[$dt['warna']] > 1) rowspan="{{$total[$dt['warna']]}}" style="vertical-align:middle;" @endif class="warna-bag-kasir-{{$dt['warna']}}">{{number_format($dt->trip_base,0,',','.')}}</td>
																							<td @if($total[$dt['warna']] > 1) rowspan="{{$total[$dt['warna']]}}" style="vertical-align:middle;" @endif class="warna-bag-kasir-{{$dt['warna']}}">{{number_format($dt->solar,0,',','.')}}</td>
																							<td @if($total[$dt['warna']] > 1) rowspan="{{$total[$dt['warna']]}}" style="vertical-align:middle;" @endif class="warna-bag-kasir-{{$dt['warna']}}">{{number_format($dt->overnight,0,',','.')}}</td>
																							<td @if($total[$dt['warna']] > 1) rowspan="{{$total[$dt['warna']]}}" style="vertical-align:middle;" @endif class="warna-bag-kasir-{{$dt['warna']}}">{{number_format($dt->multi_drop_kasir,0,',','.')}}</td>
												
																

																							<td @if($total[$dt['warna']] > 1) rowspan="{{$total[$dt['warna']]}}" style="vertical-align:middle;" @endif  class="warna-tot-sales-{{$dt['warna']}}">{{number_format($tot_cost = $dt->trip_base+$dt->solar+$dt->overnight+$dt->multi_drop_kasir,0,',','.')}}</td>
																							
																					
																							<td @if($total[$dt['warna']] > 1) rowspan="{{$total[$dt['warna']]}}" style="vertical-align:middle;" @endif class="warna-tot-sales-{{$dt['warna']}}">{{number_format($tot_sales+$tot_cost,0,',','.')}}</td>

																							<td @if($total[$dt['warna']] > 1) rowspan="{{$total[$dt['warna']]}}" style="vertical-align:middle;" @endif class="warna-remarks-{{$dt['warna']}}">{{$dt->Remark}}{{$dt->id}}</td>
																							<?php $cekinstansi=$dt['warna'];?>
																							@endif		
																					</tr>
																					<?php }?>
																					</tbody>
																					
																			</table>
																</div>
                
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@section('js')
<!-- DataTables -->
<script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
@endsection